'use strict';

angular.module('myApp.viewLogin', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewLogin', {
            templateUrl: 'viewLogin/viewLogin.html',
            controller: 'ViewLoginCtrl'
        });
    }])
    .controller('ViewLoginCtrl', ['$http', '$window', 'AuthService', '$rootScope', function ($http, $window, AuthService, $rootScope) {
        var URL = 'http://localhost:8080';
        var self = this;
        this.formUser = {
            'username': '',
            'password': ''
        };

        this.authenticate = function () {
            $http.post(URL + "/authenticate", self.formUser)
                .then(function (resp) {
                        console.log("Success: " + resp);

                        var token = resp.data.token;
                        var loggedInUser = resp.data.user;

                        // Ustawienie zalogowanego użytkownika
                        AuthService.loggedInUser = loggedInUser;

                        $http.defaults.headers.common['Authorization'] = 'Bearer ' + token;

                        $window.sessionStorage.setItem('token', token);
                        $window.sessionStorage.setItem('user_id', loggedInUser.appUserId);

                        window.location = "#!/";
                        $rootScope.loggedIn = true;
                    },
                    function (resp) {
                        console.log("Error: " + resp);
                    });
        };

    }]);