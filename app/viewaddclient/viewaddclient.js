'use strict';

angular.module('myApp.viewaddclient', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewaddclient', {
            templateUrl: 'viewaddclient/viewaddclient.html',
            controller: 'viewaddclientCtrl'
        });
    }])

    .controller('viewaddclientCtrl', ['$http', '$rootScope', '$window', 'AuthService', function ($http, $rootScope, $window, AuthService) {
        var URL = 'http://localhost:8080';
        var self = this;
        this.formClient = {
            'client_name': '',
            'phone_number': '',
            'city': '',
            'email': ''
        };
        self.loggedInUser = AuthService.loggedInUser.appUserId;

        this.sendToBackend = function () {
            var url = "name=" + self.formClient.client_name + "&" +
                "mail=" + self.formClient.email + "&" +
                "phone=" + self.formClient.phone_number + "&" +
                "city=" + self.formClient.city;

            console.log("Request: " + URL + "/clients/register?" + url);
            $http.get(URL + "/clients/register?" + url)
                .then(function (response) {
                    console.log(response.data.message);
                    $http.get(URL+"/clients/add?userid=" + self.loggedInUser + "&clientid="+response.data.message)
                        .then(function (response) {
                            console.log("Success: " + response);
                            $window.location = "/#!/viewClients";
                        },
                        function (response) {
                            console.log("Error: " + response);
                        });
                }, function (data) {
                    console.log(data);
                });
        };
    }]);