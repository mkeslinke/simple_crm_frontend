'use strict';
angular.module('myApp.viewClients', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewClients', {
            templateUrl: 'viewClients/viewClients.html',
            controller: 'viewClientsCtrl'
        });
    }])
    .controller('viewClientsCtrl', ['$http', '$rootScope', '$window', 'AuthService', function ($http , $rootScope, $window, AuthService) {
        var URL = 'http://localhost:8080';
        var self = this;
        this.clientList = [];

        this.loggedInUser = AuthService.loggedInUser.appUserId;

        this.fetchClients = function () {
            $http.get(URL + '/user/clientlist?userid=' + self.loggedInUser)
                .then(
                    function (data) {
                        console.log(data);
                        var clients = data.data;

                        self.clientList = [];

                        for (var index in clients) {
                            console.log(clients[index]);
                            self.clientList.push(clients[index]);
                        }
                    },
                    function () {
                        console.log("error");
                    }
                );
        };

        this.removeClient = function(client_to_remove){
            console.log("Remove client : " + client_to_remove);
            $http.delete(URL + '/clients/deleteclient?clientid=' + client_to_remove )

                .then(function (response) {
                        console.log("Success: " + response);
                        location.reload();
                    },
                    function (response) {
                        console.log("Error: " + response);
                    });
        }, function (data) {
            console.log(data);
        };
        self.fetchClients();
    }]);

function sortTable(n){
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("Client_table");
    switching = true;
    // Set the sorting direction to ascending:
    dir = "asc";
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
        // Start by saying: no switching is done:
        switching = false;
        rows = table.getElementsByTagName("TR");
        /* Loop through all table rows (except the
        first, which contains table headers): */
        for (i = 1; i < (rows.length - 1); i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false;
            /* Get the two elements you want to compare,
            one from current row and one from the next: */
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            /* Check if the two rows should switch place,
            based on the direction, asc or desc: */
            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch= true;
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch= true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            /* If a switch has been marked, make the switch
            and mark that a switch has been done: */
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            // Each time a switch is done, increase this count by 1:
            switchcount ++;
        } else {
            /* If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again. */
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}